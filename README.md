# README #

### What is this repository for? ###

Storing my Python codes and everything I learn about it. It's just for personal
storage and monitoring my progress. 

### Where will you learn Python?

I'm an economist, so I'll use Sargent's blog for learning. Here is the link
https://lectures.quantecon.org/. 

### Who do I talk to? ###

You can find me at my e-mail, marcelogelati@gmail.com