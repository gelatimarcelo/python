#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Sep  9 08:57:19 2017

@author: marcelodoncatto
"""

## I created a factorial functional that allow only positive integers

def fac(k):    
    if k < 0 or type(k) == float: 
        print("Use only positive integers as the input.")
    else:
        i = 1
        while k >= 1:
            i = k*i
            k = k - 1
        print(i)