#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep  8 22:59:02 2017

@author: marcelo silva
"""    
def multa(k):
    if k>80:
        valor = 1200 + (k-80)*5
        print("O usuário ultrpassou o limite de velocidade e a multa dele é de ", valor, " reais.")
    else:
        print("O usuário se livrou da multa.")


        