# Write a function that takes two sequences seq_a and seq_b as arguments and returns True if every element in seq_a is also an element of seq_b, else False

def seq_compar(a, b):
    y = []
    for i, j in list(zip(a, b)):
        if i == j:
            y.append(True)
        else:
            y.append(False)
    print(all(y))