# Consider the polynomial

# p(x)=a0+a1x+a2x2+⋯anxn = ∑i = aixi

# Write a function p such that p(x, coeff) that computes the value in (1) given a point x and a list of coefficients coeff

# Try to use enumerate() in your loop

def pol(x, coeff):
	y = []
	for index, coefficient in enumerate(coeff):
		y.append(coefficient*x**index)
	print(sum(y))