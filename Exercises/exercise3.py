# Write a function that takes a string as an argument and returns the number of capital letters in the string

def count_cap(x):
	y = []
	for i in x:
		if i == i.upper() and i != ' ':
			y.append(i)
	return len(y)

# This function doesn't work properly. If we choose a string, let's say it, "2 is the best number", our function is supposed to return 0. But '2' == '2'.upper, so our function is going to return 1.