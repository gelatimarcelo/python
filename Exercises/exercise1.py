# Part 1: Given two numeric lists or tuples x_vals and y_vals of equal length, compute their inner product using zip()

def innerprod(x, y):
    z = []
    for i, j in list(zip(x, y)):
        z.append(i*j)
    print(sum(z))

# Part 2: In one line, count the number of even numbers in 0,...,99

sum([True if x % 2 == 0 else False for x in list(range(100))])

# Part 3: Given pairs = ((2, 5), (4, 2), (9, 8), (12, 10)), count the number of pairs (a, b) such that both a and b are even

def evenpairs(x):
    y = []
    for i in x:
        if i[0] % 2 == 0 and i[1] % 2 == 0:
            y.append(i)
    print(len(y))