# When we cover the numerical libraries, we will see they include many alternatives for interpolation and function approximation

# Nevertheless, let’s write our own function approximation routine as an exercise

# In particular, without using any imports, write a function linapprox that takes as arguments

# A function f mapping some interval [a,b] into R
# two scalars a and b providing the limits of this interval
# An integer n determining the number of grid points
# A number x satisfying a <= x <= b

# and returns the piecewise linear interpolation of f at x, based on n evenly spaced grid points a = point[0] < point[1] < ... < point[n-1] = b

# Aim for clarity, not efficiency


def linterpol(f, a, b, n, x):
	cte = (b-a)/(n-1) # Creating the distance between each n and n + 1 term
	y = [a] # Creating a list starting with a
	j = 1 # Index for while operator
	while n >= j:
		y.append(a + j*cte)
		j = j + 1
	# Now i have my list y with length n with every term equidistant
	# Down here I'll create the linear function evaluated at points i, f(i) and i + 1, f(i + 1) 
	for i in y:
		if i <= x <= i + 1 and x <= b: # I want to create the linear function only when [i, i + 1] contains x
			ang_coeff = (f(i + 1) - f(i))/((i + 1) - i) # Angular coefficient of the linear function
			intercept = f(i) - ang_coeff * i # Intercept of the linear function
			return ang_coeff * x + intercept # Return linear interpolation
		else:
			pass # If [i, i + 1] doesn't contain x, do nothing






